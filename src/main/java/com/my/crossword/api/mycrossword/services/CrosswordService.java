package com.my.crossword.api.mycrossword.services;

import java.util.List;

import com.my.crossword.api.mycrossword.models.Crossword;



public interface CrosswordService {
    
    List<Crossword> findAll();
    
    // method to save
    Crossword save(Crossword crossword);

    // method to find an object by id
    Crossword findById(Long id);

}