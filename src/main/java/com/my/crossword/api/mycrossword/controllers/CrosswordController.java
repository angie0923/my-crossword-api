package com.my.crossword.api.mycrossword.controllers;

import java.util.List;

import com.my.crossword.api.mycrossword.models.Crossword;
import com.my.crossword.api.mycrossword.services.CrosswordService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1")
public class CrosswordController {
    // bring in service
    @Autowired
    CrosswordService cService;

    // method to GET data from db;
    // table named "crossword"
    @GetMapping("/crossword")
    public ResponseEntity<List<Crossword>> get() {
        List<Crossword> crossword = cService.findAll();
        return new ResponseEntity<List<Crossword>>(crossword, HttpStatus.OK);
    }

    // method to POST data
    @PostMapping("/crossword")
    public ResponseEntity<Crossword> save(@RequestBody Crossword crossword) {
        Crossword newCrossword = cService.save(crossword);
        return new ResponseEntity<Crossword>(newCrossword, HttpStatus.OK);
    

    // method to GET data of individual object
    }@GetMapping ("/crossword/{id}")
    public ResponseEntity<Crossword> getCrossword(@PathVariable("id")Long id) {
    Crossword viewCrossword = cService.findById(id);
    return new ResponseEntity<Crossword>(viewCrossword, HttpStatus.OK);
    }
    
} // end of class