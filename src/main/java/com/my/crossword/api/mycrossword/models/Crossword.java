package com.my.crossword.api.mycrossword.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "crossword")
@Getter
@Setter

public class Crossword {
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String firstLetter;

    private String secondLetter;

    private String thirdLetter;

    private String fourthLetter;

    private String fifthLetter;

    private String sixthLetter;

    private String seventhLetter;
}