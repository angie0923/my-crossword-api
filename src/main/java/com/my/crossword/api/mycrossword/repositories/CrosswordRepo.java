package com.my.crossword.api.mycrossword.repositories;

import com.my.crossword.api.mycrossword.models.Crossword;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CrosswordRepo extends JpaRepository<Crossword, Long> {
    
}
