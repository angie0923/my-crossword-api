package com.my.crossword.api.mycrossword;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyCrosswordApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyCrosswordApplication.class, args);
	}

}
